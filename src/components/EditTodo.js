import React , { useState } from 'react';
import "../styles/EditTodo.css";

function EditTodo(props) {
    const [text , setText ] = useState(props.text)
    const inputHandler = e => setText(e.target.value);
    return (
        <div className="edit-todo">
            <div className="input-edit">         <input className="input" value={text} onChange={inputHandler}/>
            </div>
        <div className="btn-edit">
            <button className="btn" type="button" onClick={() => props.edit(text)} > Save </button>
            </div>
        </div>
    )
}

export default EditTodo;