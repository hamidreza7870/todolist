import React , { Component } from 'react';
import "./styles/App.css";
import Header from "./components/Header.js"
import FormAdd from "./components/FormAdd.js"
import TodoList from "./components/TodoList.js"

class App extends Component {
    state = {
        todos : []
    }

    addTodo(text) {
        this.setState(prevState => {
            return {
                todos : [
                    ...prevState.todos,
                    { key : Date.now() , done : false , text }
                ]
            }
        })
    }

    deleteTodo(key) {
        this.setState(prevState => {
            return {
                todos : prevState.todos.filter(item => item.key !== key)
            }
        })
    }

    editTodo(key , text) {
        let { todos } = this.state;

        let item = todos.find(item => item.key === key);
        item.text = text ;

        let newTodos = todos.filter(item => item.key !== key)

        this.setState({
            todos : [
                ...newTodos,
                item
            ]
        })
    }

    toggleTodo(key) {
        let { todos } = this.state;

        let item = todos.find(item => item.key === key);
        item.done = ! item.done ;

        let newTodos = todos.filter(item => item.key !== key)

        this.setState({
            todos : [
                ...newTodos,
                item
            ]
        })
    }

    render() {
        return (
            <div className="App">
                <Header />
                <FormAdd add={this.addTodo.bind(this)} />
                <TodoList 
                          todos={this.state.todos}
                          delete={this.deleteTodo.bind(this)}
                          done={this.toggleTodo.bind(this)} 
                          edit={this.editTodo.bind(this)}
                      />
                
            </div>
        )
    }
}

export default App;