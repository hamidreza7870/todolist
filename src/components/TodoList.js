import React , { useState } from 'react';
import Todo from './Todo';
import "../styles/Todo.css"

function TodoList(props) {
    const [ statusDone , setDone ] = useState(false);
    const { todos } = props;
    const filterTodos = todos.filter(item => item.done === statusDone)
    return (
        <>
            <nav className="nav">
                <div className="nav-btn">
                    <button className="btn-undone btn-nav" onClick={() => setDone(false)}> undone <span className="count count-undone">{ todos.filter(item => item.done === false).length }</span></button>
                    <button className="btn-done btn-nav"  onClick={() => setDone(true) }> done <span className="count count-done">{ todos.filter(item => item.done === true).length}</span></button>
                </div>
            </nav>
            {
                filterTodos.length === 0
                    ?   <p className="no-todo">there isn`t any todos</p>
                    : filterTodos.map(item => <Todo 
                                                    key={item.key} 
                                                    item={item} 
                                                    delete={props.delete}
                                                    done={props.done} 
                                                    edit={props.edit}
                                                />
                                            )
            }
        </>
    )
}


export default TodoList;