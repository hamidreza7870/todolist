import React , { useState } from 'react'
import EditTodo from './EditTodo';
import "../styles/Todo.css"

function Todo(props) {
    const { item } = props;
    const [ edit , setEdit ] = useState(false);
    const editHandler = text => {
        props.edit(item.key,text);
        setEdit(false);
    }

    return (
        <div className="container">
            {
                ! edit
                    ? (
                        <div className="todo">
                            <div className="list-container">
                                <div className="item-todo">
                                    <div style={{textDecoration : item.done ? 'line-through' : 'none'}} className="title-todo">
                                        {item.text}
                                    </div>
                                    <div>
                                        <button  type="button" className="btn-option done" onClick={() => props.done(item.key)}>{ item.done ? 'undone' : 'done'}</button>
                                        <button type="button" className="btn-option edit" onClick={() => setEdit(true)}>edit</button>
                                        <button type="button" className="btn-option delete" onClick={() => props.delete(item.key)}>delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                       )  

                    : <EditTodo text={item.text} edit={editHandler}/> 
            }
            
       </div>
    )
}



export default Todo;