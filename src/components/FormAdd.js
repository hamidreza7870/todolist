import React , { useState } from 'react'
import "../styles/FormAdd.css"

function FormAddTodo(props) {
    const [ text , setText ] = useState('');
    const formHandler = e => {
        e.preventDefault();
        props.add(text);
        setText('');
    }
    const inputHandler = e => setText(e.target.value)
    return (
        <form className="form" onSubmit={formHandler}>
            <input type="text" className="input-form" placeholder="I want to do ..." value={text} onChange={inputHandler}/>
            <button type="submit" className="button-form">Add Task</button>
        </form>
    )
}


export default FormAddTodo;